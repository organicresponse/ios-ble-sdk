//
//  blelib.h
//  blelib
//
//  Created by Neil Jonathan Joaquin on 1/3/20.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for blelib.
FOUNDATION_EXPORT double blelibVersionNumber;

//! Project version string for blelib.
FOUNDATION_EXPORT const unsigned char blelibVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <blelib/PublicHeader.h
