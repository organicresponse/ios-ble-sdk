# iOS BLE SDK #

This project is to be used as a library for iOS applications. It's main use is to provide a communication interface with OR nodes.

### SETUP ###

1. Include this project's .podspec file in your podifle then pod install to install the needed dependencies for this library to work
2. Embed the .framework file in your iOS project